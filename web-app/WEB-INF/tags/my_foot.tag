<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix = "cr" uri = "/WEB-INF/tld/copyright.tld"%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    #bottom-right{
        position: absolute;
        bottom: 10px;
        right: 10px;
        color: white;
    }
</style>
<footer id="bottom-right">
    <cr:copyright/>
</footer>

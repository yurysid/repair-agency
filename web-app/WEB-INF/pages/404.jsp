<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<html>
<head>
    <style>
        #back {
            position: absolute;
            top: 12px;
            right: 20px;
            font-size: 1.5rem
        }
    </style>
    <c:choose>
        <c:when test="${sessionScope.user ne null}">
            <button id="back" class="btn-dark" onclick=location.href='/controller?command=profile'><fmt:message key="ra.back"/></button>
        </c:when>
        <c:otherwise>
            <button id="back" class="btn-dark" onclick=location.href='/'><fmt:message key="ra.back" /></button>
        </c:otherwise>
    </c:choose>
    <link rel="stylesheet" href="/style/style.css">
    <title>404</title>
</head>
<body id="body">
<div class="size404">
    404
</div>
<ft:my_foot/>
</body>
</html>

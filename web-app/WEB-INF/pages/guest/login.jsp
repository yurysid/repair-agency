<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../jspf/taglib.jspf" %>


<html>
<head>
    <title><fmt:message key="ra.logIn"/></title>
    <%@ include file="../../jspf/language.jspf" %>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
</head>
<header>
    <%@ include file="../../jspf/header.jspf" %>
</header>
<body id="body" class="align-content-center">
<div class="container">
        <form class="myForm" method="post" action="/controller?command=login">
            <br class="form-group col-xs-2">
            <label class="text-white"> <fmt:message key="ra.login"/>:
                <br/>
                <input class="input" type="text" name="login" required
                       pattern="[\wА-яёЁ].{5,}"><br/>
            </label>
            <br/>
            <label class="text-white"><fmt:message key="ra.password"/>:
                <br/>
                <input class="input" type="password" name="password" required
                       pattern="[\wА-яёЁ]].{5,}"><br/>
            </label>
            <br/>
            <button class="align-content-center btn-dark"><fmt:message key="ra.logIn"/></button>
            <button type="button" class="align-content-center btn-dark" onclick="location.href='index.jsp'"><fmt:message key="ra.back"/></button>
        </form>
</div>
<ft:my_foot/>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../jspf/taglib.jspf" %>

<html>
<head>
    <title><fmt:message key="ra.registration"/></title>
    <%@ include file="../../jspf/language.jspf" %>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
</head>
<header>
    <%@ include file="../../jspf/header.jspf" %>
</header>
<body id="body">
<div>
<form class="myForm" method="post" action="/controller?command=registration">
    <div class="form-group">
        <label class="align-content-center text-white"><fmt:message key="ra.login"/>:
            <br/>
            <input class="input type="text" name="login" required
                   pattern="[\wА-яёЁ]].{6,}"><br/>
        </label>
        <br/>
        <label class="align-content-center text-white"><fmt:message key="ra.password"/>:
            <br/>
            <input class="input" type="password" name="password" required
                   pattern="[\wА-яёЁ]].{6,}"><br/>
        </label class="align-content-center">
        <br/>
        <label class="align-content-center text-white"><fmt:message key="ra.repeatPassword"/>:
            <br/>
            <input class="input" type="password" name="password2"required
                   pattern="[\wА-яёЁ]].{6,}"><br/>
        </label>
    </div>
    <button class="align-content-center btn-dark"><fmt:message key="ra.submit"/></button>
    <button class="align-content-center btn-dark" onclick="location.href='index.jsp'"><fmt:message key="ra.back"/></button>
</form>
</div>
<ft:my_foot/>
</body>
</html>

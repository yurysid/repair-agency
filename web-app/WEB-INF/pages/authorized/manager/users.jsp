<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../../jspf/taglib.jspf" %>

<html>
<head>
    <title><fmt:message key="ra.users"/></title>
    <%@ include file="../../../jspf/language.jspf" %>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
</head>
<header>
    <%@ include file="../../../jspf/header.jspf" %>
</header>
<body id="body">
<table id="table" class="table table-striped table-bordered">
    <thead>
    <tr class="thead-dark">
        <th><fmt:message key="ra.login"/></th>
        <th><fmt:message key="ra.role"/></th>
        <th><fmt:message key="ra.moneyBalance"/></th>
    </tr>
    </thead>
    <c:forEach var="user" items="${users}">
        <tr class="thead-light">
            <th>${user.getLogin()}</th>
            <th>
                <c:choose>
                    <c:when test="${sessionScope.lang eq 'en'}">
                        ${roleMap.get(user.getRoleId()).getNameEn()}
                    </c:when>
                    <c:when test="${sessionScope.lang eq 'ru'}">
                        ${roleMap.get(user.getRoleId()).getNameRu()}
                    </c:when>
                    <c:otherwise>
                        ${roleMap.get(user.getRoleId()).getNameEn()}
                    </c:otherwise>
                </c:choose>
            </th>
            <th>${user.getMoneyBalance()}</th>
            <c:if test="${user.getRoleId() == 1}">
                <th class="thead-dark">
                <form method="post" action="/controller?command=addMoney&userId=${user.getId()}">
                    <label>
                        <input type="number" name="money">
                    </label>
                    <button class="btn-dark"><fmt:message key="ra.addMoney"/></button>
                </form>
                </th>
            </c:if>
            </div>
        </tr>
    </c:forEach>
</table>
<div>


    <table class="pagination table table-striped">
        <tr class="thead-dark">
            <c:if test="${currentPage != 1}">
                <th class="page-item thead-dark">
                    <button type="button" class="btn-dark page-link" onclick="location.href='controller?command=users&page=${currentPage - 1}'">
                        <fmt:message key="ra.previous"/>
                    </button>
                </th>
            </c:if>
            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <th class="page-item thead-dark">${i}</th>
                    </c:when>
                    <c:otherwise>
                        <th class="page-item thead-dark"><a class="page-link" href="/controller?command=users&page=${i}">${i}</a></th>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${currentPage lt noOfPages}">
                <th class="page-item thead-dark">
                    <button type="button" class="btn-dark page-link" onclick="location.href='controller?command=users&page=${currentPage + 1}'">
                        <fmt:message key="ra.next"/>
                    </button>
                </th>
            </c:if>
        </tr>
    </table>
    <button class="btn-dark" onclick="location.href='/controller?command=profile'">
        <fmt:message key="ra.backToProfile"/></button>
</div>
<ft:my_foot/>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../jspf/taglib.jspf"%>

<html>
<head>
    <style>
        #bl {
            position: absolute;
            top: 12px;
            right: 20px;
            text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
            color:white;
        }
    </style>
    <title><fmt:message key="ra.profile"/></title>
    <%@ include file="../../jspf/language.jspf"%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
    <button id="bl" class="btn-dark" onclick=location.href='/controller?command=logout'><fmt:message key="ra.logout" /></button>
</head>
<header>
    <%@ include file="../../jspf/header.jspf"%>
</header>
<body id="body">
<div class="profileForm text-white">
<c:if test="${user.getRoleId() == 1}">
    <fmt:message key="ra.hello" />, ${user.getLogin()}!
    <fmt:message key="ra.moneyBalance" />: ${user.getMoneyBalance()}
    <p>
    <form method="post" action="/controller?command=addMoney&userId=${user.getId()}">
        <label><fmt:message key="ra.addMoney" />:
            <input type="text" name="money" required pattern="\d{1,5}[\.\d]{0,3}">
        </label>
        <button class="btn-dark"><fmt:message key="ra.submit" /></button>
    </form>
    <p>
        <button class="btn-dark" onclick="location.href='/controller?command=applications&page=1'" class="btn-dark"><fmt:message key="ra.applications" /></button>
        <button class="btn-dark" onclick=location.href='/controller?command=addApplication'><fmt:message key="ra.addApplication" /></button>
    </c:if>
        <c:if test="${user.getRoleId() == 3}">
        <button id="bttMaster" onclick="location.href='/controller?command=applications&page=1'" class="btn-dark"><fmt:message key="ra.applications" /></button>
        </c:if>
<c:if test="${user.getRoleId() == 2}">
        <label id="btt">
    <button onclick="location.href='/controller?command=users&page=1'" class="btn-dark"><fmt:message key="ra.users" /></button>
            <button onclick="location.href='/controller?command=applications&page=1'" class="btn-dark"><fmt:message key="ra.applications" /></button>
    </label>
    </c:if>
</div>
<ft:my_foot/>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../../jspf/taglib.jspf" %>

<html>
<head>
    <style>
        #addForm {
            min-width: 50px;
            position: absolute;
            text-align: center;
            top: 30%;
            left: 12%;
            /*transform: translate(-50%, -50%);*/
            font-size: 1.5rem
        }
         #back {
             position: absolute;
             top: 12px;
             right: 20px;;
         }
    </style>
    <title><fmt:message key="ra.addApplication"/></title>
    <%@ include file="../../../jspf/language.jspf" %>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
    <button id="back" class="float-right btn-dark" onclick=location.href='/controller?command=profile'><fmt:message key="ra.back"/></button>
</head>
<header>
    <%@ include file="../../../jspf/header.jspf" %>
</header>
<body id="body" class="align-content-center">
<div id="addForm" class="container">
    <div>
        <div class="form-group text-white">
        <form method="post" action="/controller?command=addApplication">
                <label class="align-content-center"><fmt:message key="ra.description"/>:
                    <input type="text" name="description" required pattern="\w+">
                    <br/>
                    <p>
                    <label class="align-content-center"><fmt:message key="ra.category"/>:
                    <select name="techniqueCategoryId">
                        <c:forEach var="techniqueCategory" items="${techniqueCategories}">
                            <c:choose>
                                <c:when test="${sessionScope.lang eq 'en'}">
                                    <option value="${techniqueCategory.getId()}">${techniqueCategory.getNameEn()}</option>
                                </c:when>
                                <c:when test="${sessionScope.lang eq 'ru'}">
                                    <option value="${techniqueCategory.getId()}">${techniqueCategory.getNameRu()}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${techniqueCategory.getId()}">${techniqueCategory.getNameEn()}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    </label>
            </label>
            <button class="btn-dark"><fmt:message key="ra.submit"/></button>
        </form>
    </div>
</div>
</div>
<ft:my_foot/>
</body>
</html>

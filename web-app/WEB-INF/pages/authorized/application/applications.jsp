<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../../jspf/taglib.jspf" %>

<html>
<head>
    <title><fmt:message key="ra.yourApplications"/></title>
    <%@ include file="../../../jspf/language.jspf" %>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
</head>
<body id="body">
<header>
    <%@ include file="../../../jspf/header.jspf" %>
</header>
<p>
<c:if test="${user.getRoleId() == 2}">
    <table id="tableFilter" class="table table-striped table-bordered">
        <tr class="thead-dark">
                <th>
                    <form method="post" action="/controller?command=applications&filterBy=master&page=1">
                        <label><fmt:message key="ra.filterByMaster"/>:
                            <select name="filterAttributeId" onchange="this.form.submit()">
                                <option></option>
                                <c:forEach var="master" items="${masters}">
                                    <option value=${master.getId()}>${master.getLogin()}</option>
                                </c:forEach>
                            </select
                        </label>
                    </form>
                </th>
                <th>
                    <form class="thead-dark" method="post" action="/controller?command=applications&filterBy=status&page=1">
                        <label  class="thead-dark"><fmt:message key="ra.filterByStatus"/>:
                            <select class="thead-dark" name="filterAttributeId" onchange="this.form.submit()">
                                <option></option>
                                <c:forEach var="status" items="${statuses}">
                                    <option value=${status.getId()}>
                                        <c:choose>
                                            <c:when test="${sessionScope.lang eq 'en'}">
                                                ${status.getNameEn()}
                                            </c:when>
                                            <c:when test="${sessionScope.lang eq 'ru'}">
                                                ${status.getNameRu()}
                                            </c:when>
                                            <c:otherwise>
                                                ${status.getNameEn()}
                                            </c:otherwise>
                                        </c:choose>
                                    </option>
                                </c:forEach>
                            </select>
                        </label>
                    </form>
                </th>
                <c:if test="${param.filterBy ne null}">
                    <th>
                        <button type="button" onclick=location.href='/controller?command=applications&page=1'>
                            <fmt:message key="ra.resetFilter"/>
                        </button>
                    </th>
                </c:if>
        </tr>
    </table>
</c:if>

    <table id="table" class="table table-light table-striped table-bordered text-dark">
        <thead>
        <tr class="thead-dark">
            <th><fmt:message key="ra.category"/></th>
            <th><fmt:message key="ra.description"/></th>
            <th>
                <c:choose>
                    <c:when test="${param.sortBy == 'cost' && param.orderBy == 'asc'}">
                        <a href="/controller?command=applications&sortBy=cost&orderBy=desc&page=1"><fmt:message
                                key="ra.cost"/> ↑</a>
                    </c:when>
                    <c:when test="${param.sortBy == 'cost' && param.orderBy == 'desc'}">
                        <a href="/controller?command=applications&page=1"><fmt:message
                                key="ra.cost"/> ↓</a>
                    </c:when>
                    <c:otherwise>
                        <a href="/controller?command=applications&sortBy=cost&orderBy=asc&page=1"><fmt:message
                                key="ra.cost"/></a>
                    </c:otherwise>
                </c:choose>
            </th>
            <th>
                <c:choose>
                    <c:when test="${param.sortBy == 'date' && param.orderBy == 'desc'}">
                        <a href="/controller?command=applications&page=1"><fmt:message
                                key="ra.date"/> ↓</a>
                    </c:when>
                    <c:otherwise>
                        <a href="/controller?command=applications&sortBy=date&orderBy=desc&page=1"><fmt:message
                                key="ra.date"/></a>
                    </c:otherwise>
                </c:choose>
            </th>
            <th>
                <c:choose>
                    <c:when test="${param.sortBy == 'status_id' && param.orderBy == 'asc'}">
                        <a href="/controller?command=applications&sortBy=status_id&orderBy=desc&page=1"><fmt:message
                                key="ra.status"/> ↑</a>
                    </c:when>
                    <c:when test="${param.sortBy == 'status_id' && param.orderBy == 'desc'}">
                        <a href="/controller?command=applications&page=${currentPage}"><fmt:message
                                key="ra.status"/> ↓</a>
                    </c:when>
                    <c:otherwise>
                        <a href="/controller?command=applications&sortBy=status_id&orderBy=asc&page=1"><fmt:message
                                key="ra.status"/></a>
                    </c:otherwise>
                </c:choose>
            </th>
            <th><fmt:message key="ra.review"/></th>
        </tr>
        </thead>

        <c:forEach var="application" items="${applications}">
            <tr class="thead-dark">
                <td>
                    <c:choose>
                        <c:when test="${sessionScope.lang eq 'en'}">
                            ${techniqueCategoryMap.get(application.getTechniqueCategoryId()).getNameEn()}
                        </c:when>
                        <c:when test="${sessionScope.lang eq 'ru'}">
                            ${techniqueCategoryMap.get(application.getTechniqueCategoryId()).getNameRu()}
                        </c:when>
                        <c:otherwise>
                            ${techniqueCategoryMap.get(application.getTechniqueCategoryId()).getNameEn()}
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>${application.getDescription()}</td>
                <td>
                    <c:choose>
                        <c:when test="${user.getRoleId() == 2 && application.getStatusId() == 1}">
                            <form method="post"
                                  action="/controller?command=setApplicationCost&applicationId=${application.getId()}">
                                <label>
                                    <input type="number" name="cost">
                                </label>
                                <button><fmt:message key="ra.setCost"/></button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            ${application.getCost()}
                            <c:if test="${user.getRoleId() == 1 && application.getStatusId() == 2}">
                                <form method="post"
                                      action="/controller?command=payApplication&applicationId=${application.getId()}">
                                    <button><fmt:message key="ra.pay"/></button>
                                </form>
                            </c:if>
                            <c:if test="${user.getRoleId() == 1 && application.getStatusId() == 2}">
                                <form method="post"
                                      action="/controller?command=changeApplicationStatus&applicationId=${application.getId()}&statusId=4">
                                    <button><fmt:message key="ra.cancel"/></button>
                                </form>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>${application.getDate()}</td>
                <td>
                    <c:choose>
                        <c:when test="${sessionScope.lang eq 'en'}">
                            ${statusMap.get(application.getStatusId()).getNameEn()}
                        </c:when>
                        <c:when test="${sessionScope.lang eq 'ru'}">
                            ${statusMap.get(application.getStatusId()).getNameRu()}
                        </c:when>
                        <c:otherwise>
                            ${statusMap.get(application.getStatusId()).getNameEn()}
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${user.getRoleId() == 2 && application.getStatusId() == 3}">
                    <form method="post"
                          action="/controller?command=setMaster&applicationId=${application.getId()}&statusId=5">
                        <label>
                            <select name="masterId">
                                <option></option>
                                <c:forEach var="master" items="${masters}">
                                    <option value=${master.getId()}>${master.getLogin()}</option>
                                </c:forEach>
                            </select>
                        </label>
                        <button><fmt:message key="ra.setMaster"/></button>
                        </c:if>
                        <c:if test="${user.getRoleId() == 3 && application.getStatusId() == 5}">
                            <form method="post"
                                  action="/controller?command=changeApplicationStatus&applicationId=${application.getId()}&statusId=6">
                                <button><fmt:message key="ra.setInProgress"/></button>
                            </form>
                        </c:if>
                        <c:if test="${user.getRoleId() == 3 && application.getStatusId() == 6}">
                            <form method="post"
                                  action="/controller?command=changeApplicationStatus&applicationId=${application.getId()}&statusId=7">
                                <button><fmt:message key="ra.setCompleted"/></button>
                            </form>
                        </c:if>
                    </form>
                </td>

                <td>
                    <c:if test="${application.getReviewId() != 0}">
                        ${reviewMap.get(application.getReviewId()).getContent()}
                    </c:if>
                    <c:if test="${user.getRoleId() == 1 && application.getStatusId() == 7 && application.getReviewId() == 0}">
                        <form method="post" action="/controller?command=addReview&applicationId=${application.getId()}">
                            <label>
                                <input type="text" name="content"><br/>
                            </label>
                            <button><fmt:message key="ra.leaveReview"/></button>
                        </form>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>

    <table class="pagination table table-striped">
        <tr class="thead-dark">
            <c:if test="${currentPage != 1}">
                <th class="page-item thead-dark">
                    <c:if test="${param.sortBy == null && param.filterBy == null}">
                        <button class="btn-dark page-link" type="button"
                                onclick="location.href='/controller?command=applications&page=${currentPage - 1}'">
                            <fmt:message key="ra.previous"/>
                        </button>
                    </c:if>
                    <c:if test="${param.sortBy != null}">
                        <button class="btn-dark page-link" type="button"
                                onclick="location.href='/controller?command=applications&sortBy=${param.sortBy}&orderBy=${param.orderBy}&page=${currentPage - 1}'">
                            <fmt:message key="ra.previous"/>
                        </button>
                    </c:if>
                    <c:if test="${param.filterBy != null}">
                        <button class="btn-dark page-link" type="button"
                                onclick="location.href='/controller?command=applications&filterBy=${param.filterBy}&filterAttributeId=${param.filterAttributeId}&page=${currentPage - 1}'">
                            <fmt:message key="ra.previous"/>
                        </button>
                    </c:if>
                </th>
            </c:if>
            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <th class="thead-dark">${i}</th>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${param.sortBy == null && param.filterBy == null}">
                            <th class="thead-dark"><a class="page-link" href="/controller?command=applications&page=${i}">${i}</a></th>
                        </c:if>
                        <c:if test="${param.sortBy != null}">
                            <th class="thead-dark"><a class="page-link"
                                   href="/controller?command=applications&sortBy=${param.sortBy}&orderBy=${param.orderBy}&page=${i}">${i}</a>
                            </th>
                        </c:if>
                        <c:if test="${param.filterBy != null}">
                            <th class="thead-dark">
                                <a class="page-link"
                                   href="/controller?command=applications&filterBy=${param.filterBy}&filterAttributeId=${param.filterAttributeId}&page=${i}">${i}</a>
                            </th>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${currentPage lt noOfPages}">
                <th class="thead-dark">
                    <c:if test="${param.sortBy == null && param.filterBy == null}">
                        <button class="btn-dark page-link" type="button"
                                onclick="location.href='/controller?command=applications&page=${currentPage + 1}'">
                            <fmt:message key="ra.next"/>
                        </button>
                    </c:if>
                    <c:if test="${param.sortBy != null}">
                        <button class="btn-dark page-link" type="button"
                                onclick="location.href='/controller?command=applications&sortBy=${param.sortBy}&orderBy=${param.orderBy}&page=${currentPage + 1}'">
                            <fmt:message key="ra.next"/>
                        </button>
                    </c:if>
                    <c:if test="${param.filterBy != null}">
                        <button class="btn-dark page-link" type="button"
                                onclick="location.href='/controller?command=applications&filterBy=${param.filterBy}&filterAttributeId=${param.filterAttributeId}&page=${currentPage + 1}'">
                            <fmt:message key="ra.next"/>
                        </button>
                    </c:if>
                </th>
            </c:if>
        </tr>
    </table>
    <p>
        <button class="btn-dark" onclick="location.href='/controller?command=profile'">
            <fmt:message key="ra.backToProfile"/></button>
        <ft:my_foot/>
</body>
</html>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="WEB-INF/jspf/taglib.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><fmt:message key="ra.repairAgency"/></title>
    <%@ include file="WEB-INF/jspf/language.jspf" %>
    <link rel="stylesheet" href="/style/bootstrap.css">
    <link rel="stylesheet" href="/style/style.css">
</head>
<header>
    <%@ include file="WEB-INF/jspf/header.jspf" %>
</header>

<body id="body">
    <div class="myForm">
        <p>
            <c:choose>
            <c:when test="${user eq null}">
                <button style="padding: 23px" class="btn-dark" onclick="location.href='/controller?command=login'"><fmt:message key="ra.logIn" /></button>
                <button style="padding: 23px" class="btn-dark" onclick="location.href='/controller?command=registration'"><fmt:message key="ra.signUp" /></button>
            </c:when>
                <c:otherwise>
                <button style="padding: 50px; margin-top: 80px; margin-left: 40px; " class="btn-dark" onclick="location.href='/controller?command=profile'"><fmt:message key="ra.profile" /></button>
                    </c:otherwise>
                </c:choose>
       </div>
<ft:my_foot/>
</body>
</html>

package agency.web.controller;

import agency.web.command.Command;
import agency.web.command.CommandHandler;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * FrontControllerServlet
 *
 * @author Y.Sydorenko
 *
 */

public class FrontControllerServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(FrontControllerServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("Front controller started execution 'doGet'.");
        process(req, resp);
        log.debug("Front controller finished execution 'doGet'.");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.debug("Front controller started execution 'doPost'.");
        process(req, resp);
        log.debug("Front controller finished execution 'doPost'.");
    }


    /**
     *
     * The method gets command from request and initialize it with CommandHandler
     * After that execute concrete command
     *
     */
    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("Front controller started execution 'process'.");
        Command command = CommandHandler.get(req.getParameter("command"));
        command.execute(req, resp);
        log.debug("Front controller started execution 'process'.");
    }
}

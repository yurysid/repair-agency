package agency.web.constant;

/**
 * Pages Paths
 *
 * @author Y.Sydorenko
 *
 */

public enum PagePath {
    PAGE__ADD_APPLICATION("WEB-INF/pages/authorized/application/addApplication.jsp"),
    PAGE__APPLICATIONS("WEB-INF/pages/authorized/application/applications.jsp"),
    PAGE__USERS("WEB-INF/pages/authorized/manager/users.jsp"),
    PAGE__PROFILE("WEB-INF/pages/authorized/profile.jsp"),
    PAGE__LOGIN("WEB-INF/pages/guest/login.jsp");

    private final String path;

    PagePath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}

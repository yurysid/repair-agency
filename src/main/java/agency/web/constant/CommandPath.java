package agency.web.constant;

/**
 * Commands Paths
 *
 * @author Y.Sydorenko
 *
 */

public enum CommandPath {
    COMMAND__ADD_APPLICATION("controller?command=addApplication"),
    COMMAND__GET_APPLICATIONS("controller?command=applications&page="),
    COMMAND__GET_USERS("controller?command=users&page="),
    COMMAND__PROFILE("controller?command=profile");

    private final String path;

    CommandPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}

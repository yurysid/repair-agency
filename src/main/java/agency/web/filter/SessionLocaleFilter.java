package agency.web.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * SessionLocaleFilter
 *
 * @author Y.Sydorenko
 *
 */

@WebFilter("/*")
public class SessionLocaleFilter implements Filter {
    private static final Logger log = Logger.getLogger(SessionLocaleFilter.class);

    /**
     * Receives a language parameter from request and then set it as locale if it is valid.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.debug("Session locale filter started.");
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        if (req.getParameter("lang") != null) {
            req.getSession().setAttribute("lang", req.getParameter("lang"));
            log.debug("Attribute lang has been set in the session.");
        }
        log.debug("Session locale filter finished.");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}

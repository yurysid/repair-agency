package agency.web.filter;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import agency.db.model.User;
import org.apache.log4j.Logger;

/**
 * AddApplicationCommand
 *
 * @author Y.Sydorenko
 *
 */

public class CommandAccessFilter implements Filter {
    private static final Logger log = Logger.getLogger(CommandAccessFilter.class);

    private static final Map<Integer, List<String>> accessMap = new HashMap<>();
    private static List<String> authorizedList = new ArrayList<>();
    private static List<String> guestList = new ArrayList<>();

     /**
      * Checks if current user is allowed to access the page.
      * Otherwise redirects to a home page.
      */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.debug("Filter starts");

        if (accessAllowed(request)) {
            log.debug("Filter finished");
            chain.doFilter(request, response);
        } else {
            HttpServletResponse servletResponse = (HttpServletResponse) response;
            servletResponse.sendRedirect("/");
            log.debug("CommandAccessFilter did redirect on '/'");
        }
    }

    /**
     * @return true if user is allowed to access the page and
     * false if user is not allowed to access the page;
     *
     */

    private boolean accessAllowed(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String commandName = request.getParameter("command");

        HttpSession session = httpRequest.getSession();
        User user = (User) session.getAttribute("user");

        if (guestList.contains(commandName) && user == null) {
            return true;
        }

        if (user == null) {
            return false;
        }

        if(authorizedList.contains(commandName)){
            return true;
        }

        return accessMap.get(user.getRoleId()).contains(commandName);
    }

    /**
     * Receives filter config from web.xml and put it's parameter to collections.
     */

    public void init(FilterConfig fConfig) {
        accessMap.put(1, asList(fConfig.getInitParameter("client")));
        accessMap.put(2, asList(fConfig.getInitParameter("manager")));
        accessMap.put(3, asList(fConfig.getInitParameter("master")));
        authorizedList = asList(fConfig.getInitParameter("authorized"));
        guestList = asList(fConfig.getInitParameter("guest"));
    }

    /**
     * Extracts parameter values from String.
     *
     * @param string Parameter values String
     * @return list of parameter values
     */
    private List<String> asList(String string) {
        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(string);
        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }
        return list;
    }
}

package agency.web.listener;

import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;

/**
 * ContextListener
 *
 * @author Y.Sydorenko
 *
 */

public class ContextListener implements ServletContextListener {

    /**
     * Initializes log4j.
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        String log4jConfigFile = servletContext.getInitParameter("log4j-config-location");
        String fullPath = servletContext.getRealPath("") + File.separator + log4jConfigFile;

        PropertyConfigurator.configure(fullPath);
    }
}

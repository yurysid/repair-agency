package agency.web.command.guest;

import agency.db.dao.UserDAO;
import agency.db.model.User;
import agency.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AddApplicationCommand
 *
 * @author Y.Sydorenko
 *
 */

public class RegistrationCommand implements Command {
    private static final Logger log = Logger.getLogger(RegistrationCommand.class);


    /**
     * The method for registration.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("RegistrationCommand has been started.");
        UserDAO userDAO = new UserDAO();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");

        if (login == null || login.isEmpty() || password == null || password.isEmpty() || password2 == null || password2.isEmpty()) {
            log.debug("RegistrationCommand did forward on 'registration.jsp'");
            request.getRequestDispatcher("WEB-INF/pages/guest/registration.jsp").forward(request, response);
        } else {
            if (userDAO.getUserByLogin(login) == null && password.equals(password2)) {
                User user = new User();
                user.setLogin(request.getParameter("login"));
                user.setPassword(request.getParameter("password"));

                userDAO.insert(user);
                log.debug("RegistrationCommand did redirect on '/'");
                response.sendRedirect("/");
            } else {
                log.debug("RegistrationCommand did forward on 'registration.jsp'");
                request.getRequestDispatcher("WEB-INF/pages/guest/registration.jsp").forward(request, response);
            }
        }
        log.debug("RegistrationCommand has been finished.");
    }
}

package agency.web.command.guest;

import agency.db.dao.UserDAO;
import agency.db.model.User;
import agency.web.command.Command;
import agency.web.constant.CommandPath;
import agency.web.constant.PagePath;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AddApplicationCommand
 *
 * @author Y.Sydorenko
 *
 */

public class LoginCommand implements Command {
    private static final Logger log = Logger.getLogger(LoginCommand.class);



    /**
     * The method for logging in;
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("LoginCommand has been started.");
        String forward = PagePath.PAGE__LOGIN.getPath();

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            log.debug("LoginCommand did forward on: " + forward);
            request.getRequestDispatcher(forward).forward(request, response);
        } else {
            User user = new UserDAO().getUserByLogin(login);
            if (user == null || !password.equals(user.getPassword())) {
                log.debug("LoginCommand did forward on: " + forward);
                request.getRequestDispatcher(forward).forward(request, response);
            } else {
                forward = CommandPath.COMMAND__PROFILE.getPath();
                request.getSession().setAttribute("user", user);
                log.debug("LoginCommand did redirect on " + forward);
                response.sendRedirect(forward);
            }
        }
        log.debug("LoginCommand has been finished.");
    }
}

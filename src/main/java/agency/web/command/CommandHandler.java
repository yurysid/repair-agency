package agency.web.command;

import agency.web.command.authorized.LogoutCommand;
import agency.web.command.authorized.ProfileCommand;
import agency.web.command.authorized.GetUserApplicationsCommand;
import agency.web.command.authorized.manager.SetApplicationCostCommand;
import agency.web.command.authorized.client.AddApplicationCommand;
import agency.web.command.authorized.ChangeApplicationStatusCommand;
import agency.web.command.authorized.client.AddReviewCommand;
import agency.web.command.authorized.client.PayApplicationCommand;
import agency.web.command.authorized.manager.SetMasterCommand;
import agency.web.command.authorized.AddMoneyCommand;
import agency.web.command.authorized.manager.GetUsersCommand;
import agency.web.command.guest.LoginCommand;
import agency.web.command.guest.RegistrationCommand;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * AddApplicationCommand
 *
 * @author Y.Sydorenko
 *
 */

public class CommandHandler {
    private static final Logger log = Logger.getLogger(CommandHandler.class);
    private static final Map<String, Command> commands = new HashMap<>();

    static {
        commands.put("registration", new RegistrationCommand());
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("profile", new ProfileCommand());

        commands.put("addMoney", new AddMoneyCommand());
        commands.put("users", new GetUsersCommand());

        commands.put("addApplication", new AddApplicationCommand());
        commands.put("applications", new GetUserApplicationsCommand());
        commands.put("changeApplicationStatus", new ChangeApplicationStatusCommand());
        commands.put("setApplicationCost", new SetApplicationCostCommand());
        commands.put("payApplication", new PayApplicationCommand());
        commands.put("setMaster", new SetMasterCommand());

        commands.put("addReview", new AddReviewCommand());
    }

    public static Command get(String commandName) {
        log.debug("CommandHandler returned the next command: " + commandName);
        return commands.get(commandName);
    }
}

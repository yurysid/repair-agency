package agency.web.command;

import agency.web.constant.CommandPath;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Interface Command
 *
 * @author Y.Sydorenko
 *
 */

public interface Command {

    /**
     * Processes requested parameters and forwards/redirects to a certain page.
     */
    void execute(HttpServletRequest request,
                   HttpServletResponse response) throws ServletException, IOException;


    /**
     * The method redirects on '/controller?command=application'
     * with possible adding a session attribute.
     */

    default void applicationsRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(request.getSession().getAttribute("filterBy") != null){
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getSession().getAttribute("page") + "&filterBy=" + request.getSession().getAttribute("filterBy") + "&filterAttributeId=" + Integer.parseInt(request.getSession().getAttribute("filterAttributeId").toString()));
        } else if(request.getSession().getAttribute("sortBy") != null) {
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getSession().getAttribute("page") + "&sortBy=" + request.getSession().getAttribute("sortBy") + "&orderBy=" + request.getSession().getAttribute("orderBy"));
        } else {
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getSession().getAttribute("page"));
        }
    }
}

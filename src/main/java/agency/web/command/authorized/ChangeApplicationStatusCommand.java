package agency.web.command.authorized;

import agency.db.dao.ApplicationDAO;
import agency.db.dao.StatusDAO;
import agency.db.model.Application;
import agency.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ChangeApplicationStatusCommand
 *
 * @author Y.Sydorenko
 *
 */

public class ChangeApplicationStatusCommand implements Command {
    private static final Logger log = Logger.getLogger(ChangeApplicationStatusCommand.class);


    /**
     * The method of changing an application status depending on the user role and user's command
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("ChangeApplicationStatusCommand has been started.");
        if (request.getParameter("applicationId") != null) {
            int applicationId = Integer.parseInt(request.getParameter("applicationId"));

            ApplicationDAO applicationDAO = new ApplicationDAO();
            Application application = applicationDAO.getApplicationById(applicationId);

            int statusId = new StatusDAO().getStatusById(Integer.parseInt(request.getParameter("statusId"))).getId();
            application.setStatusId(applicationDAO.changeApplicationStatus(application, statusId));
            request.getSession().setAttribute("application", application);
        }
        applicationsRedirect(request, response);
        log.debug("ChangeApplicationStatusCommand has been finished.");
    }
}

package agency.web.command.authorized;

import agency.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * LogoutCommand
 *
 * @author Y.Sydorenko
 *
 */

public class LogoutCommand implements Command {
    private static final Logger log = Logger.getLogger(LogoutCommand.class);


    /**
     * The method for logging out.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("LogoutCommand has been started.");
        HttpSession session = request.getSession();
        if(session != null){
            session.invalidate();
            log.debug("LogoutCommand invalidated session.");
        }
        log.debug("LogoutCommand did redirect on '/'");
        response.sendRedirect("/");
        log.debug("Logout command has been finished.");
    }
}

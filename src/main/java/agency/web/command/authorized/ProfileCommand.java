package agency.web.command.authorized;

import agency.web.command.Command;
import agency.web.constant.PagePath;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ProfileCommand
 *
 * @author Y.Sydorenko
 *
 */

public class ProfileCommand implements Command {
    private static final Logger log = Logger.getLogger(ProfileCommand.class);


    /**
     * The method shows user's profile.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("ProfileCommand has been started.");
        log.debug("ProfileCommand did forward on " + PagePath.PAGE__PROFILE.getPath());
        request.getRequestDispatcher(PagePath.PAGE__PROFILE.getPath()).forward(request, response);
        log.debug("ProfileCommand has been finished.");
    }
}

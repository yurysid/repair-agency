package agency.web.command.authorized.client;

import agency.db.dao.ApplicationDAO;
import agency.db.dao.TechniqueCategoryDAO;
import agency.db.model.Application;
import agency.db.model.User;
import agency.db.model.TechniqueCategory;
import agency.web.command.Command;
import agency.web.constant.CommandPath;
import agency.web.constant.PagePath;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * AddApplicationCommand
 *
 * @author Y.Sydorenko
 *
 */

public class AddApplicationCommand implements Command {
    private static final Logger log = Logger.getLogger(AddApplicationCommand.class);


    /**
     * The method of adding an application by a client.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("AddApplicationCommand has been started.");
        TechniqueCategoryDAO techniqueCategoryDAO = new TechniqueCategoryDAO();
        List<TechniqueCategory> techniqueCategories = techniqueCategoryDAO.getAll();
        request.setAttribute("techniqueCategories", techniqueCategories);

        if (request.getParameter("techniqueCategoryId") == null || request.getParameter("description") == null
            || request.getParameter("techniqueCategoryId").isEmpty() || request.getParameter("description").isEmpty()) {
            request.getRequestDispatcher(PagePath.PAGE__ADD_APPLICATION.getPath()).forward(request, response);
            log.debug("AddApplicationCommand did forward on " + PagePath.PAGE__ADD_APPLICATION.getPath());
        } else {
            User user = (User) request.getSession().getAttribute("user");

            Application application = new Application();
            application.setTechniqueCategoryId(techniqueCategoryDAO.getTechniqueCategoryById(Integer.parseInt(request.getParameter("techniqueCategoryId"))).getId());
            application.setDescription(request.getParameter("description"));

            ApplicationDAO applicationDAO = new ApplicationDAO();
            applicationDAO.insert(application);
            applicationDAO.addApplicationToUser(applicationDAO.getApplicationById(application.getId()), user);
            response.sendRedirect(CommandPath.COMMAND__ADD_APPLICATION.getPath());
            log.debug("AddApplicationCommand did redirect on " + CommandPath.COMMAND__ADD_APPLICATION.getPath());
        }
        log.debug("AddApplicationCommand has been started.");
    }
}

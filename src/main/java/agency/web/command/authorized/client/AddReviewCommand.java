package agency.web.command.authorized.client;

import agency.db.dao.ApplicationDAO;
import agency.db.dao.ReviewDAO;
import agency.db.model.Review;
import agency.web.command.Command;
import agency.web.constant.CommandPath;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AddReviewCommand
 *
 * @author Y.Sydorenko
 *
 */


public class AddReviewCommand implements Command {
    private static final Logger log = Logger.getLogger(AddReviewCommand.class);


    /**
     * The method of adding a review by a client.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("AddReviewCommand has been started.");
        ApplicationDAO applicationDAO = new ApplicationDAO();

        if (request.getParameter("applicationId") == null
                && applicationDAO.getAll().contains(applicationDAO.getApplicationById(Integer.parseInt(request.getParameter("applicationId"))))) {
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getParameter("page"));
            log.debug("AddReviewCommand did redirect on " + CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getParameter("page"));
        } else if(applicationDAO.getApplicationById(Integer.parseInt(request.getParameter("applicationId"))).getStatusId() != 7){
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getParameter("page"));
            log.debug("AddReviewCommand did redirect on " + CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getParameter("page"));
        } else if (request.getParameter("content") == null || request.getParameter("content").isEmpty()) {
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getParameter("page"));
            log.debug("AddReviewCommand did redirect on " + CommandPath.COMMAND__GET_APPLICATIONS.getPath() + request.getParameter("page"));
        } else {
            int applicationId = Integer.parseInt(request.getParameter("applicationId"));
            Review review = new Review();
            review.setContent(request.getParameter("content"));
            ReviewDAO reviewDAO = new ReviewDAO();
            reviewDAO.insert(review);


            applicationDAO.addReviewToApplication(reviewDAO.getReviewById(review.getId()), applicationDAO.getApplicationById(applicationId));
            applicationsRedirect(request, response);
        }
        log.debug("AddReviewCommand has been finished.");
    }
}

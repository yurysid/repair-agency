package agency.web.command.authorized.client;

import agency.db.dao.ApplicationDAO;
import agency.db.dao.UserDAO;
import agency.db.model.Application;
import agency.db.model.User;
import agency.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * PayApplicationCommand
 *
 * @author Y.Sydorenko
 *
 */

public class PayApplicationCommand implements Command {
    private static final Logger log = Logger.getLogger(PayApplicationCommand.class);


    /**
     * The method of paying the client's application by the client.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("PayApplicationCommand has been started.");
        if (request.getParameter("applicationId") != null) {
            User user = (User) request.getSession().getAttribute("user");
            int applicationId = Integer.parseInt(request.getParameter("applicationId"));

            ApplicationDAO applicationDAO = new ApplicationDAO();
            Application application = applicationDAO.getApplicationById(applicationId);

            UserDAO userDAO = new UserDAO();

            if (userDAO.payApplication(user, application)) {
                application.setStatusId(applicationDAO.changeApplicationStatus(application, 3));
                user = userDAO.getUserByLogin(user.getLogin());
                request.getSession().setAttribute("user", user);
                request.getSession().setAttribute("application", application);
            }
        }
        applicationsRedirect(request, response);
        log.debug("PayApplicationCommand has been finished.");
    }
}

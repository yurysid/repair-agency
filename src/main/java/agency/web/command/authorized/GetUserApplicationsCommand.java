package agency.web.command.authorized;

import agency.db.dao.*;
import agency.db.model.*;
import agency.web.command.Command;

import agency.web.constant.CommandPath;
import agency.web.constant.PagePath;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * GetUserApplicationsCommand
 *
 * @author Y.Sydorenko
 *
 */

public class GetUserApplicationsCommand implements Command {
    private static final Logger log = Logger.getLogger(GetUserApplicationsCommand.class);
    private static final List<String> SORTING_PARAMS = new ArrayList<>();
    private static final List<String> ORDER_PARAMS = new ArrayList<>();

    static {
        SORTING_PARAMS.add("date");
        SORTING_PARAMS.add("status_id");
        SORTING_PARAMS.add("cost");

        ORDER_PARAMS.add("asc");
        ORDER_PARAMS.add("desc");
    }


    /**
     * The method shows user's applications (if user is a client or a master)
     * or shows all application if user is a manager
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("GetUserApplicationCommand has been started.");
        User user = (User) request.getSession().getAttribute("user");
        ApplicationDAO applicationDAO = new ApplicationDAO();
        if ((user.getRoleId() != 2 && applicationDAO.getUserApplications(user).isEmpty()) || (user.getRoleId() == 2 && applicationDAO.getAll().isEmpty())) {
            log.debug("GetUserApplicationCommand did redirect on " + CommandPath.COMMAND__PROFILE.getPath());
            response.sendRedirect(CommandPath.COMMAND__PROFILE.getPath());
        } else if ((request.getParameter("page") == null) || (!request.getParameter("page").matches("^[1-9]+[0-9]*$"))
                || (!SORTING_PARAMS.contains(request.getParameter("sortBy")) && request.getParameter("sortBy") != null)
                || (!ORDER_PARAMS.contains(request.getParameter("orderBy")) && request.getParameter("orderBy") != null)
                || (request.getParameter("sortBy") == null && request.getParameter("orderBy") != null)) {
            log.debug("GetUserApplicationCommand did redirect on " + CommandPath.COMMAND__PROFILE.getPath());
            response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + 1);
        } else {
            int page = 1;
            int recordsPerPage = 5;
            int noOfPages = 0;

            if (request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }

            request.getSession().removeAttribute("filterBy");
            request.getSession().removeAttribute("filterAttributeId");
            request.getSession().removeAttribute("sortBy");
            request.getSession().removeAttribute("orderedBy");

            List<Application> applicationsPerPage = new ArrayList<>();

            String sortBy = request.getParameter("sortBy");
            String orderBy = request.getParameter("orderBy");

            String filterBy = request.getParameter("filterBy");
            String filterAttributeId = request.getParameter("filterAttributeId");

            if (user.getRoleId() == 2) {
                List<User> masters = new UserDAO().getAllMasters();
                request.getSession().setAttribute("masters", masters);

                List<Status> statuses = new StatusDAO().getAll();
                request.setAttribute("statuses", statuses);
            }

            if (sortBy != null && orderBy != null) {
                if (user.getRoleId() == 2) {
                    applicationsPerPage = applicationDAO.getAllSortedPerPage((page - 1) * recordsPerPage, recordsPerPage, sortBy, orderBy);
                    noOfPages = (int) Math.ceil((double) applicationDAO.getAll().size() / recordsPerPage);
                } else {
                    applicationsPerPage = applicationDAO.getSortedUserApplicationsPerPage((page - 1) * recordsPerPage, recordsPerPage, user, sortBy, orderBy);
                    noOfPages = (int) Math.ceil((double) applicationDAO.getUserApplications(user).size() / recordsPerPage);
                }
                request.getSession().setAttribute("orderBy", orderBy);
                request.setAttribute("orderBy", orderBy);
                request.getSession().setAttribute("sortBy", sortBy);
            } else if (user.getRoleId() == 2 && filterBy != null && filterAttributeId != null) {
                request.getSession().setAttribute("filterAttributeId", filterAttributeId);
                request.getSession().setAttribute("filterBy", filterBy);
                if (filterBy.equals("master")) {
                    User master = new UserDAO().getUserById(Integer.parseInt(filterAttributeId));
                    applicationsPerPage = applicationDAO.getUserApplicationsPerPage((page - 1) * recordsPerPage, recordsPerPage, master);
                    noOfPages = (int) Math.ceil((double) applicationDAO.getUserApplications(master).size() / recordsPerPage);
                } else if (filterBy.equals("status")) {
                    Status status = new StatusDAO().getStatusById(Integer.parseInt(filterAttributeId));
                    applicationsPerPage = applicationDAO.getAllApplicationsByStatusPerPage((page - 1) * recordsPerPage, recordsPerPage, status);
                    noOfPages = (int) Math.ceil((double) applicationDAO.getAllApplicationsByStatus(status).size() / recordsPerPage);
                }
                request.setAttribute("filterAttributeId", filterAttributeId);
            } else {
                if (user.getRoleId() == 2) {
                    List<User> masters = new UserDAO().getAllMasters();
                    request.setAttribute("masters", masters);

                    List<Status> statuses = new StatusDAO().getAll();
                    request.setAttribute("statuses", statuses);

                    applicationsPerPage = applicationDAO.getAllPerPage((page - 1) * recordsPerPage, recordsPerPage);
                    noOfPages = (int) Math.ceil((double) applicationDAO.getAll().size() / recordsPerPage);
                } else {
                    applicationsPerPage = applicationDAO.getUserApplicationsPerPage((page - 1) * recordsPerPage, recordsPerPage, user);
                    noOfPages = (int) Math.ceil((double) applicationDAO.getUserApplications(user).size() / recordsPerPage);
                }
            }

            TechniqueCategoryDAO techniqueCategoryDAO = new TechniqueCategoryDAO();
            StatusDAO statusDAO = new StatusDAO();
            ReviewDAO reviewDAO = new ReviewDAO();

            Map<Integer, TechniqueCategory> techniqueCategoryMap = techniqueCategoryDAO.getAll().stream()
                    .collect(Collectors.toMap(Entity::getId, Function.identity()));

            Map<Integer, Status> statusMap = statusDAO.getAll().stream()
                    .collect(Collectors.toMap(Entity::getId, Function.identity()));

            Map<Integer, Review> reviewMap = reviewDAO.getAll().stream()
                    .collect(Collectors.toMap(Entity::getId, Function.identity()));

            request.setAttribute("techniqueCategoryMap", techniqueCategoryMap);
            request.setAttribute("statusMap", statusMap);
            request.setAttribute("reviewMap", reviewMap);

            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("currentPage", page);

            request.setAttribute("applications", applicationsPerPage);
            request.getSession().setAttribute("page", page);

            if (noOfPages < Integer.parseInt(request.getParameter("page"))) {
                response.sendRedirect(CommandPath.COMMAND__GET_APPLICATIONS.getPath() + 1);
                log.debug("GetUserApplicationCommand did redirect on " + CommandPath.COMMAND__GET_APPLICATIONS.getPath() + 1);
            } else {
                request.getRequestDispatcher(PagePath.PAGE__APPLICATIONS.getPath()).forward(request, response);
                log.debug("GetUserApplicationCommand did forward on " + PagePath.PAGE__APPLICATIONS.getPath());
            }
        }
        log.debug("GetUserApplicationCommand has been finished.");
    }
}


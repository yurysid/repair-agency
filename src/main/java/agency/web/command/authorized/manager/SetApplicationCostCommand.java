package agency.web.command.authorized.manager;

import agency.db.dao.ApplicationDAO;
import agency.db.model.Application;
import agency.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * SetApplicationCostCommand
 *
 * @author Y.Sydorenko
 *
 */

public class SetApplicationCostCommand implements Command {
    private static final Logger log = Logger.getLogger(SetApplicationCostCommand.class);


    /**
     * The method of setting application cost by a manager.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("SetApplicationCostCommand has been started.");
        ApplicationDAO applicationDAO = new ApplicationDAO();

        if (request.getParameter("applicationId") != null && request.getParameter("cost") != null
                && !request.getParameter("cost").isEmpty() && !(Double.parseDouble(request.getParameter("cost")) < 1)
                && applicationDAO.getAll().contains(applicationDAO.getApplicationById(Integer.parseInt(request.getParameter("applicationId"))))) {

                String cost = request.getParameter("cost");
                int applicationId = Integer.parseInt(request.getParameter("applicationId"));
                Application application = applicationDAO.getApplicationById(applicationId);
                applicationDAO.setApplicationCost(application, Double.parseDouble(cost));
                request.getSession().setAttribute("applications", applicationDAO.getAll());
        }
        applicationsRedirect(request, response);
        log.debug("SetApplicationCostCommand has been finished.");
    }
}

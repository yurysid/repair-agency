package agency.web.command.authorized.manager;

import agency.db.dao.ApplicationDAO;
import agency.db.dao.UserDAO;
import agency.db.model.Application;
import agency.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * SetMasterCommand
 *
 * @author Y.Sydorenko
 *
 */

public class SetMasterCommand implements Command {
    private static final Logger log = Logger.getLogger(SetMasterCommand.class);


    /**
     * The method of setting application to a master by a manager.
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("SetMasterCommand has been started.");
        UserDAO userDAO = new UserDAO();
        if (request.getParameter("applicationId") != null
                && request.getParameter("statusId") != null
                && request.getParameter("masterId") != null && !request.getParameter("masterId").isEmpty()
                && userDAO.getAllMasters().contains(userDAO.getUserById(Integer.parseInt(request.getParameter("masterId"))))) {
            ApplicationDAO applicationDAO = new ApplicationDAO();

            int applicationId = Integer.parseInt(request.getParameter("applicationId"));
            Application application = applicationDAO.getApplicationById(applicationId);

            int statusId = Integer.parseInt(request.getParameter("statusId"));

            application.setStatusId(applicationDAO.changeApplicationStatus(application, statusId));
            request.getSession().setAttribute("application", application);

            int masterId = Integer.parseInt(request.getParameter("masterId"));
            applicationDAO.addApplicationToUser(applicationDAO.getApplicationById(application.getId()), new UserDAO().getUserById(masterId));
            request.getSession().setAttribute("applications", applicationDAO.getAll());
        }
        applicationsRedirect(request, response);
        log.debug("SetMasterCommand has been finished.");
    }
}

package agency.web.command.authorized.manager;

import agency.db.dao.RoleDAO;
import agency.db.dao.UserDAO;
import agency.db.model.User;
import agency.db.model.Role;
import agency.web.command.Command;
import agency.web.constant.CommandPath;
import agency.web.constant.PagePath;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * GetUsersCommand
 *
 * @author Y.Sydorenko
 *
 */

public class GetUsersCommand implements Command {
    private static final Logger log = Logger.getLogger(GetUsersCommand.class);


    /**
     * The method shows all users (available only for manager)
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("GetUsersCommand has been started.");


        if(request.getParameter("page") == null || !request.getParameter("page").matches("^[1-9]+[0-9]*$")) {
            response.sendRedirect(CommandPath.COMMAND__GET_USERS.getPath() + 1);
            log.debug("GetUsersCommand did redirect on" + CommandPath.COMMAND__GET_USERS.getPath() + 1);
        } else {
            List<Role> roles = new RoleDAO().getAll();
            request.getSession().setAttribute("roles", roles);

            int page = 1;
            int recordsPerPage = 5;
            if(request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            UserDAO userDAO = new UserDAO();
            List<User> usersPerPage = userDAO.getUsersPerPage((page - 1) * recordsPerPage, recordsPerPage);
            int noOfPages = (int) Math.ceil((double)userDAO.getAll().size() / recordsPerPage);

            Map<Integer, Role> roleMap = new HashMap<>();
            RoleDAO roleDAO = new RoleDAO();

            usersPerPage.forEach
                    (userPerPage -> roleMap.put(userPerPage.getRoleId(), roleDAO.getRoleById(userPerPage.getRoleId())));

            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("currentPage", page);
            if(request.getSession().getAttribute("users") == null) {
                request.setAttribute("users", usersPerPage);
            }
            request.setAttribute("roleMap", roleMap);
            request.getSession().setAttribute("page", page);

            if(noOfPages < Integer.parseInt(request.getParameter("page"))){
                response.sendRedirect(CommandPath.COMMAND__GET_USERS.getPath() + 1);
                log.debug("GetUsersCommand did redirect on " + CommandPath.COMMAND__GET_USERS.getPath() + 1);
            } else {
                request.getRequestDispatcher(PagePath.PAGE__USERS.getPath()).forward(request, response);
                log.debug("GetUsersCommand did redirect on " + PagePath.PAGE__USERS.getPath());
            }
        }
        log.debug("GetUsersCommand has been started.");
    }
}

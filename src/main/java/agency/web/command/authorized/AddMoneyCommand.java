package agency.web.command.authorized;

import agency.db.dao.RoleDAO;
import agency.db.dao.UserDAO;
import agency.db.model.User;
import agency.db.model.Role;
import agency.web.command.Command;
import agency.web.constant.CommandPath;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * AddMoneyCommand
 *
 * @author Y.Sydorenko
 *
 */

public class AddMoneyCommand implements Command {
    private static final Logger log = Logger.getLogger(AddMoneyCommand.class);


    /**
     * The method of adding money on user's money balance. (not available for master)
     */

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("AddMoneyCommand has been started");
        User user = (User) request.getSession().getAttribute("user");
        UserDAO userDAO = new UserDAO();
        double money;
        if (request.getParameter("money") != null && !request.getParameter("money").isEmpty() && Double.parseDouble(request.getParameter("money")) > 0) {
            money = Double.parseDouble(request.getParameter("money"));
            if (user.getRoleId() == 1) {
                user.setMoneyBalance(userDAO.changeMoneyBalance(user, user.getMoneyBalance() + money));
                response.sendRedirect(CommandPath.COMMAND__PROFILE.getPath());
            } else if (user.getRoleId() == 2) {
                int id = Integer.parseInt(request.getParameter("userId"));
                List<Role> roles = new RoleDAO().getAll();
                request.getSession().setAttribute("roles", roles);
                userDAO.changeMoneyBalance(userDAO.getUserById(id), userDAO.getUserById(id).getMoneyBalance() + money);
                response.sendRedirect(CommandPath.COMMAND__GET_USERS.getPath() + request.getSession().getAttribute("page"));
            }
        } else {
            if (user.getRoleId() == 1) {
                response.sendRedirect(CommandPath.COMMAND__PROFILE.getPath());
                log.debug("AddMoneyCommand has been finished.");
            } else if (user.getRoleId() == 2) {
                response.sendRedirect(CommandPath.COMMAND__GET_USERS.getPath() + request.getSession().getAttribute("page"));
                log.debug("AddMoneyCommand did redirect on " + CommandPath.COMMAND__GET_USERS.getPath() + request.getSession().getAttribute("page"));
            }
        }
        request.getSession().setAttribute("user", user);
        log.debug("AddMoneyCommand has been finished.");
    }
}
package agency.db.dao;

import agency.db.DBManager;
import agency.db.model.Application;
import agency.db.model.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User DAO.
 *
 * @author Y.Sydorenko
 *
 */

public class UserDAO {
    private static final Logger log = Logger.getLogger(UserDAO.class);

    public void insert(User user) {
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users (login, pass) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.executeUpdate();
            try(ResultSet resultSet = statement.getGeneratedKeys()){
                if (resultSet.next()) {
                    user.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO insert error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
    }

    public User getUserById(int id){
        Connection connection = null;
        User user = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setRoleId(resultSet.getInt("user_role_id"));
                user.setMoneyBalance(resultSet.getDouble("money_balance"));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO getUserById error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return user;
    }

    public User getUserByLogin(String login) {
        Connection connection = null;
        User user = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login = ?");
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = createUser(resultSet);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO getUserByLogin error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return user;
    }

    public List<User> getAll() {
        Connection connection = null;
        List<User> users = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
            ResultSet resultSet = statement.executeQuery();
            users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(createUser(resultSet));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO getAll error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return users;
    }


    /**
     *
     * @param start = a start point of getting users from DB
     * @param total = amount of applications per page
     * The method returns the concrete amount of users per page
     * @return List of Users
     *
     */

    public List<User> getUsersPerPage(int start,int total) {
        Connection connection = null;
        List<User> users = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users LIMIT ?, ?");
            statement.setInt(1, start);
            statement.setInt(2, total);
            ResultSet resultSet = statement.executeQuery();
            users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(createUser(resultSet));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO getUsersPerPage error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return users;
    }

    /**
     *
     * The method returns all users whose role 'master'
     * @return List of Users
     *
     */

    public List<User> getAllMasters() {
        Connection connection = null;
        List<User> masters = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE user_role_id = 3");
            ResultSet resultSet = statement.executeQuery();
            masters = new ArrayList<>();
            while (resultSet.next()) {
                User master = new User();
                master.setId(resultSet.getInt("id"));
                master.setLogin(resultSet.getString("login"));
                master.setRoleId(resultSet.getInt("user_role_id"));
                masters.add(master);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO getAllMasters error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return masters;
    }

    /**
     *
     * @param user = the user that money balance will be changed
     * @param money = amount of money that will change user's money balance
     * The method of topping up the user's money balance
     *
     */

    public Double changeMoneyBalance(User user, double money){
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("UPDATE users SET money_balance = ? WHERE id = ?");
            statement.setDouble(1, money);
            statement.setInt(2, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO changeMoneyBalance error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return money;
    }

    /**
     *
     * @param user = client who pays his application
     * @param application = client's application that client pays
     * The method of the application payment with user's money balance
     * @return true if user can pay an application, and vice versa.
     *
     */

    public boolean payApplication(User user, Application application){
        Connection connection = null;
        boolean isPaid = false;
        try {
            if(user.getMoneyBalance() >= application.getCost()) {
                connection = DBManager.getInstance().getConnection();
                PreparedStatement statement = connection.prepareStatement("UPDATE users SET money_balance = ? WHERE id = ?");
                statement.setDouble(1, user.getMoneyBalance() - application.getCost());
                statement.setInt(2, user.getId());
                statement.executeUpdate();
                isPaid = true;
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("UserDAO payApplication error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isPaid;
    }


    /**
     *
     * @param resultSet = resultSet for setting user's params
     * The method create a user and fills his params
     *
     */

    private User createUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("pass"));
        user.setRoleId(resultSet.getInt("user_role_id"));
        user.setMoneyBalance(resultSet.getDouble("money_balance"));
        return user;
    }
}

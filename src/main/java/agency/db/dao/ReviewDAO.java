package agency.db.dao;

import agency.db.DBManager;
import agency.db.model.Review;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Review DAO.
 *
 * @author Y.Sydorenko
 *
 */

public class ReviewDAO {
    private static final Logger log = Logger.getLogger(ReviewDAO.class);

    public void insert(Review review){
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO reviews (content) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, review.getContent());
            statement.executeUpdate();
            try(ResultSet resultSet = statement.getGeneratedKeys()){
                if (resultSet.next()) {
                    review.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ReviewDAO insert error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
    }

    public Review getReviewById(int reviewId){
        Connection connection = null;
        Review review = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("SELECT * FROM reviews WHERE id = ?");
            statement.setInt(1, reviewId);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                review = new Review();
                review.setId(resultSet.getInt("id"));
                review.setContent(resultSet.getString("content"));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ReviewDAO getReviewById error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return review;
    }
    public List<Review> getAll() {
        Connection connection = null;
        List<Review> reviews = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM reviews");
            ResultSet resultSet = statement.executeQuery();
            reviews = new ArrayList<>();
            while (resultSet.next()){
                Review review = new Review();
                review.setId(resultSet.getInt("id"));
                review.setContent(resultSet.getString("content"));
                reviews.add(review);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ReviewDAO getAll error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return reviews;
    }
}

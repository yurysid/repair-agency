package agency.db.dao;

import agency.db.DBManager;
import agency.db.model.Role;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Role DAO.
 *
 * @author Y.Sydorenko
 *
 */

public class RoleDAO {
    private static final Logger log = Logger.getLogger(RoleDAO.class);

    public Role getRoleById(int id){
        Connection connection = null;
        Role role = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM user_roles WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                role = new Role();
                role.setId(resultSet.getInt("id"));
                role.setNameEn(resultSet.getString("name_en"));
                role.setNameRu(resultSet.getString("name_ru"));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("RoleDAO getRoleById error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return role;
    }

    public List<Role> getAll() {
        Connection connection = null;
        List<Role> roles = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM user_roles");
            ResultSet resultSet = statement.executeQuery();
            roles = new ArrayList<>();
            while (resultSet.next()){
                Role role = new Role();
                role.setId(resultSet.getInt("id"));
                role.setNameEn(resultSet.getString("name_en"));
                role.setNameRu(resultSet.getString("name_ru"));
                roles.add(role);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("RoleDAO getAll error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return roles;
    }
}

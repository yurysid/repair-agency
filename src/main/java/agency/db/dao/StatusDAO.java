package agency.db.dao;

import agency.db.DBManager;
import agency.db.model.Status;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Status DAO.
 *
 * @author Y.Sydorenko
 *
 */

public class StatusDAO {
    private static final Logger log = Logger.getLogger(StatusDAO.class);

    public Status getStatusById(int id){
        Connection connection = null;
        Status status = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM statuses WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                status = new Status();
                status.setId(resultSet.getInt("id"));
                status.setNameEn(resultSet.getString("name_en"));
                status.setNameRu(resultSet.getString("name_ru"));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("StatusDAO getStatusById error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return status;
    }

    public List<Status> getAll() {
        Connection connection = null;
        List<Status> statuses = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM statuses");
            ResultSet resultSet = statement.executeQuery();
            statuses = new ArrayList<>();
            while (resultSet.next()){
                Status status = new Status();
                status.setId(resultSet.getInt("id"));
                status.setNameEn(resultSet.getString("name_en"));
                status.setNameRu(resultSet.getString("name_ru"));
                statuses.add(status);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("StatusDAO getAll error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return statuses;
    }
}

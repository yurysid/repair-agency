package agency.db.dao;

import agency.db.DBManager;
import agency.db.model.TechniqueCategory;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Technique category DAO.
 *
 * @author Y.Sydorenko
 *
 */

public class TechniqueCategoryDAO {
    private static final Logger log = Logger.getLogger(TechniqueCategoryDAO.class);

    public TechniqueCategory getTechniqueCategoryById(int id){
        Connection connection = null;
        TechniqueCategory techniqueCategory = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM technique_categories WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                techniqueCategory = new TechniqueCategory();
                techniqueCategory.setId(resultSet.getInt("id"));
                techniqueCategory.setNameEn(resultSet.getString("name_en"));
                techniqueCategory.setNameRu(resultSet.getString("name_ru"));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("TechniqueCategoryDAO getTechniqueCategoryById error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return techniqueCategory;
    }

    public List<TechniqueCategory> getAll() {
        Connection connection = null;
        List<TechniqueCategory> techniqueCategories = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM technique_categories");
            ResultSet resultSet = statement.executeQuery();
            techniqueCategories = new ArrayList<>();
            while (resultSet.next()){
                TechniqueCategory techniqueCategory = new TechniqueCategory();
                techniqueCategory.setId(resultSet.getInt("id"));
                techniqueCategory.setNameEn(resultSet.getString("name_en"));
                techniqueCategory.setNameRu(resultSet.getString("name_ru"));
                techniqueCategories.add(techniqueCategory);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("TechniqueCategoryDAO getAll error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return techniqueCategories;
    }
}

package agency.db.dao;

import agency.db.DBManager;
import agency.db.model.Application;
import agency.db.model.Review;
import agency.db.model.Status;
import agency.db.model.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Application DAO.
 *
 * @author Y.Sydorenko
 *
 */

public class ApplicationDAO {
    private static final Logger log = Logger.getLogger(ApplicationDAO.class);

    public void insert(Application application){
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO applications (technique_category_id, description) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, String.valueOf(application.getTechniqueCategoryId()));
            statement.setString(2, application.getDescription());
            statement.executeUpdate();
            try(ResultSet resultSet = statement.getGeneratedKeys()){
                if (resultSet.next()) {
                    application.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO insert error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
    }

    public Application getApplicationById(int applicationId){
        Connection connection = null;
        Application application = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("SELECT * FROM applications WHERE id = ?");
            statement.setInt(1, applicationId);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                application = new Application();
                application.setId(resultSet.getInt("id"));
                application.setDescription(resultSet.getString("description"));
                application.setCost(resultSet.getDouble("cost"));
                application.setTechniqueCategoryId(resultSet.getInt("technique_category_id"));
                application.setDate(resultSet.getDate("date"));
                application.setStatusId(resultSet.getInt("status_id"));
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getApplicationById error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return application;
    }


    /**
     *
     * @param application = the application that will be added to the user
     * @param user = the user for who the application will be added
     * The method of adding the application to the user
     * in the intermediate table.
     *
     */

    public void addApplicationToUser(Application application, User user){
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("INSERT INTO users_applications (u_a_user_id, u_a_application_id) VALUES (?, ?)");
            statement.setInt(1, user.getId());
            statement.setInt(2, application.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO addApplicationToUser error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
    }

    public List<Application> getAll() {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM applications");
            ResultSet resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getAll error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }

    /**
     *
     * @param start = a start point of getting application from DB
     * @param total = amount of applications per page
     * The method return all applications
     * for correct paging calculation.
     *
     */

    public List<Application> getAllPerPage(int start, int total) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM applications LIMIT ?, ?");
            statement.setInt(1, start);
            statement.setInt(2, total);
            ResultSet resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getAllPerPage error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }

    /**
     *
     * @param start = a start point of getting application from DB
     * @param total = amount of applications per page
     * @param sortBy = parameter of sorting
     * @param orderBy = ASC or DESC
     * The method return all applications
     * for correct paging calculation and sort these applications by parameter.
     *
     */

    public List<Application> getAllSortedPerPage(int start, int total, String sortBy, String orderBy) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement;
            ResultSet resultSet;
            if(orderBy.toUpperCase().equals("ASC")){
                statement = connection.prepareStatement("SELECT * FROM applications a ORDER BY " + sortBy + " LIMIT ?, ?");
            } else {
                statement = connection.prepareStatement("SELECT * FROM applications a ORDER BY " + sortBy + " DESC LIMIT ?, ?");
            }
            statement.setInt(1, start);
            statement.setInt(2, total);
            resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getAllSortedPerPage error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }


    /**
     *
     * @param status = status from filter
     * The method return applications with concrete status.
     *
     */

    public List<Application> getAllApplicationsByStatus(Status status) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM applications WHERE status_id = ?");
            statement.setInt(1, status.getId());
            ResultSet resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getAllApplicationsByStatus error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }


    /**
     *
     * @param start = a start point of getting application from DB
     * @param total = amount of applications per page
     * @param status = status from filter
     * The method return applications with concrete status per page.
     *
     */

    public List<Application> getAllApplicationsByStatusPerPage(int start, int total, Status status) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM applications WHERE status_id = ? LIMIT ?, ?");
            statement.setInt(1, status.getId());
            statement.setInt(2, start);
            statement.setInt(3, total);
            ResultSet resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getAllApplicationsByStatusPerPage error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }


    /**
     *
     * @param user = the ser who gets his applications
     * The method return all user's applications.
     *
     */

    public List<Application> getUserApplications(User user) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM applications a JOIN users_applications ua ON a.id = ua.u_a_application_id JOIN users u ON ua.u_a_user_id = u.id WHERE u.id = ?");
            statement.setInt(1, user.getId());
            applications = new ArrayList<>();
            try (ResultSet resultSet = statement.executeQuery()) {
                fillApplications(applications, resultSet);
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getUserApplications error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }

    /**
     *
     * @param start = a start point of getting application from DB
     * @param total = amount of applications per page
     * @param user = the user who gets his applications
     * The method return all user's applications per page.
     *
     */

    public List<Application> getUserApplicationsPerPage(int start, int total, User user) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM applications a JOIN users_applications ua ON a.id = ua.u_a_application_id JOIN users u ON ua.u_a_user_id = u.id WHERE u.id = ? LIMIT ?, ?");
            statement.setInt(1, user.getId());
            statement.setInt(2, start);
            statement.setInt(3, total);
            ResultSet resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getUserApplicationsPerPage error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }



    /**
     *
     * @param start = a start point of getting application from DB
     * @param total = amount of applications per page
     * @param user = the user who gets his applications
     * @param sortBy = a parameter of sorting
     * @param orderBy = ASC or DESC
     * The method return all user's applications per page and sort them by parameter.
     *
     */

    public List<Application> getSortedUserApplicationsPerPage(int start, int total, User user, String sortBy, String orderBy) {
        Connection connection = null;
        List<Application> applications = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement;
            if(orderBy.toUpperCase().equals("ASC")){
                statement = connection.prepareStatement("SELECT * FROM applications a JOIN users_applications ua ON a.id = ua.u_a_application_id JOIN users u ON ua.u_a_user_id = u.id WHERE u.id = ? ORDER BY " + sortBy + " LIMIT ? , ?");
            } else {
                statement = connection.prepareStatement("SELECT * FROM applications a JOIN users_applications ua ON a.id = ua.u_a_application_id JOIN users u ON ua.u_a_user_id = u.id WHERE u.id = ? ORDER BY " + sortBy + " DESC LIMIT ? , ?");
            }
            statement.setInt(1, user.getId());
            statement.setInt(2, start);
            statement.setInt(3, total);
            ResultSet resultSet = statement.executeQuery();
            applications = new ArrayList<>();
            fillApplications(applications, resultSet);
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO getSortedUserApplicationsPerPage error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return applications;
    }


    /**
     *
     * @param application = the application status of which user wants to change
     * @param statusId = a status id which user wants to set to an application
     * The method can change an application's status.
     *
     */

    public int changeApplicationStatus(Application application, int statusId){
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("UPDATE applications SET status_id = ? WHERE id = ?", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, statusId);
            statement.setInt(2, application.getId());
            statement.executeUpdate();
            try(ResultSet resultSet = statement.getGeneratedKeys()){
                if (resultSet.next()) {
                    application.setId(resultSet.getInt("id"));
                }
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO changeApplicationStatus error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return statusId;
    }


    /**
     *
     * @param application = the application that manager set cost
     * @param cost = the cost that manager sets to the application
     * The method set application's cost which user has to pay for repairing.
     *
     */

    public void setApplicationCost(Application application, double cost) {
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE applications SET cost = ?, status_id = 2 WHERE id = ?", Statement.RETURN_GENERATED_KEYS);
            statement.setDouble(1, cost);
            statement.setInt(2, application.getId());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    application.setId(resultSet.getInt("id"));
                }
            }
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO setApplicationCost error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
    }


    /**
     *
     * @param review = the review that client set to it's done application
     * @param application = client's application
     * The method of adding the review to the application.
     *
     */

    public void addReviewToApplication(Review review, Application application){
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement  statement = connection.prepareStatement("UPDATE applications SET review_id = ? WHERE id = ?");
            statement.setInt(1, review.getId());
            statement.setInt(2, application.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(connection);
            log.error("ApplicationDAO addReviewToApplication error", e);
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
    }

    /**
     *
     * @param applications = applications for filling
     * @param resultSet = resultSet for iterating
     * The method fills the list of application from parameter with Application entity setters
     *
     */

    private void fillApplications(List<Application> applications, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            Application application = new Application();
            application.setId(resultSet.getInt("id"));
            application.setTechniqueCategoryId(resultSet.getInt("technique_category_id"));
            application.setDescription(resultSet.getString("description"));
            application.setDate(resultSet.getDate("date"));
            application.setCost(resultSet.getDouble("cost"));
            application.setStatusId(resultSet.getInt("status_id"));
            application.setReviewId(resultSet.getInt("review_id"));
            applications.add(application);
        }
    }
}

package agency.db.model;

import java.util.Objects;

/**
 * User entity.
 *
 * @author Y.Sydorenko
 *
 */

public class User extends Entity {
    private String login;
    private String password;
    private int roleId;
    private double moneyBalance;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public double getMoneyBalance() {
        return moneyBalance;
    }

    public void setMoneyBalance(double moneyBalance) {
        this.moneyBalance = moneyBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return Double.compare(user.moneyBalance, moneyBalance) == 0 &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                roleId == user.roleId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), login, password, roleId, moneyBalance);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", roleId=" + roleId +
                ", moneyBalance=" + moneyBalance +
                '}';
    }
}

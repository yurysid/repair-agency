package agency.db.model;
import java.util.Date;
import java.util.Objects;

/**
 * Application entity.
 *
 * @author Y.Sydorenko
 *
 */

public class Application extends Entity {
    private int techniqueCategoryId;
    private String description;
    private int statusId;
    private double cost;
    private Date date;
    private int reviewId;

    public int getTechniqueCategoryId() {
        return techniqueCategoryId;
    }

    public void setTechniqueCategoryId(int techniqueCategoryId) {
        this.techniqueCategoryId = techniqueCategoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Application application = (Application) o;
        return Double.compare(application.cost, cost) == 0 &&
                techniqueCategoryId == application.techniqueCategoryId &&
                Objects.equals(description, application.description) &&
                statusId == application.statusId &&
                Objects.equals(date, application.date) &&
                Objects.equals(reviewId, application.reviewId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), techniqueCategoryId, description, statusId, cost, date, reviewId);
    }
}

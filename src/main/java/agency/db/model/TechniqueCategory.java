package agency.db.model;

import java.util.Objects;

/**
 * Technique category entity.
 *
 * @author Y.Sydorenko
 *
 */

public class TechniqueCategory extends Entity {
    private String nameEn;
    private String nameRu;

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechniqueCategory that = (TechniqueCategory) o;
        return Objects.equals(nameEn, that.nameEn) &&
                Objects.equals(nameRu, that.nameRu);
    }
    @Override
    public int hashCode() {
        return Objects.hash(nameEn, nameRu);
    }

    @Override
    public String toString() {
        return "TechniqueCategory{" +
                "nameEn='" + nameEn + '\'' +
                ", nameRu='" + nameRu + '\'' +
                '}';
    }
}

package agency.db.model;

import java.util.Objects;

/**
 * Role entity.
 *
 * @author Y.Sydorenko
 *
 */

public class Role extends Entity {
    private String nameEn;
    private String nameRu;

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Role role = (Role) o;
        return Objects.equals(nameEn, role.nameEn) &&
                Objects.equals(nameRu, role.nameRu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nameEn, nameRu);
    }
}

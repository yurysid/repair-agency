package agency.db;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * DB manager. Works with MySQL DB.
 *
 * @author Y.Sydorenko
 *
 */

public class DBManager {
    private static final Logger log = Logger.getLogger(DBManager.class);

    private static volatile DBManager instance;

    private DBManager() {
    }

    public static DBManager getInstance() {
        if (instance == null)
            synchronized (DBManager.class) {
                if (instance == null) {
                    instance = new DBManager();
                }
            }
        return instance;
    }

    /**
     * Returns a DB connection from the Pool Connections.
     */

    public Connection getConnection() throws SQLException {
        Connection connection = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");

            DataSource ds = (DataSource) envContext.lookup("jdbc/repair_agency");

            connection = ds.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
            log.error("Error in DBManager connection.");
        }
        return connection;
    }


    public void commitAndClose(Connection con) {
        if(con != null) {
            try {
                con.commit();
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                log.error("Error in DBManager commit and close.");
            }
        }
    }

    public void rollbackAndClose(Connection con) {
        if(con != null) {
            try {
                con.rollback();
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                log.error("Error in DBManager rollback and close.");
            }
        }
    }
}

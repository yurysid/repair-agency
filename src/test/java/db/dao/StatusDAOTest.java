package db.dao;

import agency.db.DBManager;
import agency.db.dao.StatusDAO;
import agency.db.model.Status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManager.class)
public class StatusDAOTest {
    DBManager dbManager;
    Connection connection;
    PreparedStatement statement;
    ResultSet resultSet;

    @Before
    public void setUp() throws SQLException {
        dbManager = mock(DBManager.class);
        connection = mock(Connection.class);
        statement = mock(PreparedStatement.class);
        statement = mock(PreparedStatement.class);
        resultSet = mock(ResultSet.class);
        mockStatic(DBManager.class);

        when(DBManager.getInstance()).thenReturn(dbManager);
        when(dbManager.getConnection()).thenReturn(connection);
        doNothing().when(dbManager).rollbackAndClose(connection);
        doNothing().when(dbManager).commitAndClose(connection);

        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(statement);

        when(statement.executeQuery()).thenReturn(resultSet);

        doNothing().when(statement).setInt(anyInt(), anyInt());
        doNothing().when(statement).setString(anyInt(), anyString());

        when(statement.executeQuery()).thenReturn(resultSet);
        when(statement.getGeneratedKeys()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(resultSet.getString(anyString())).thenReturn("status");
        when(resultSet.getInt(anyString())).thenReturn(1);
    }

    @Test
    public void getAllTest() {
        StatusDAO statusDAO = new StatusDAO();

        statusDAO.getStatusById(1);
        statusDAO.getAll();
    }
}
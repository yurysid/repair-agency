package db.model;

import agency.db.model.Role;
import agency.db.model.Status;
import org.junit.Test;

import static org.junit.Assert.*;

public class StatusTest {
    @Test
    public void initApp(){
        Status status = new Status();
        status.setId(1);
        assertEquals(1, status.getId());
        status.setNameEn("en");
        assertEquals("en", status.getNameEn());
        status.setNameRu("ru");
        assertEquals("ru", status.getNameRu());

        assertEquals(status, status);
        assertNotEquals(0, status.hashCode());
        assertNotEquals(status, new Status());
    }
}

package db.model;

import agency.db.model.Application;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class ApplicationTest {
    @Test
    public void initApp(){
        Application application = new Application();
        application.setId(1);
        assertEquals(1, application.getId());
        application.setDescription("desc");
        assertEquals("desc", application.getDescription());
        application.setTechniqueCategoryId(1);
        assertEquals(1, application.getTechniqueCategoryId());
        application.setCost(1);
        assertEquals(1, (int) application.getCost());
        application.setStatusId(1);
        assertEquals(1, application.getStatusId());
        application.setReviewId(1);
        assertEquals(1, application.getReviewId());
        application.setDate(new Date());
        assertNotNull(application.getDate());

        assertEquals(application, application);
        assertNotEquals(0, application.hashCode());

        assertNotEquals(application, new Application());
    }
}

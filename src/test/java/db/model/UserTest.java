package db.model;

import agency.db.model.Role;
import agency.db.model.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {
    @Test
    public void initUserTest(){
        User user = new User();
        user.setId(1);
        assertEquals(1, user.getId());
        user.setRoleId(1);
        assertEquals(1, user.getRoleId());
        user.setLogin("a");
        assertEquals("a", user.getLogin());
        user.setPassword("a");
        assertEquals("a", user.getPassword());
        user.setMoneyBalance(1);
        assertEquals(1, (int) user.getMoneyBalance());

        assertEquals(user, user);
        assertNotEquals(0, user.hashCode());

        assertNotEquals(user, new User());
    }
}

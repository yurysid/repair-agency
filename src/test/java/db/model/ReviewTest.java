package db.model;

import agency.db.model.Application;
import agency.db.model.Review;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReviewTest {
    @Test
    public void initReviewTest(){
        Review review = new Review();
        review.setId(1);
        assertEquals(1, review.getId());
        review.setContent("cont");
        assertEquals("cont", review.getContent());

        assertEquals(review, review);
        assertNotEquals(0, review.hashCode());

        assertNotEquals(review, new Review());
    }
}

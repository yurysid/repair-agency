package db.model;

import agency.db.model.Role;
import agency.db.model.TechniqueCategory;
import org.junit.Test;

import static org.junit.Assert.*;

public class TechniqueCategoryTest {
    @Test
    public void initStatusTest(){
        TechniqueCategory techniqueCategory = new TechniqueCategory();
        techniqueCategory.setId(1);
        assertEquals(1, techniqueCategory.getId());
        techniqueCategory.setNameEn("en");
        assertEquals("en", techniqueCategory.getNameEn());
        techniqueCategory.setNameRu("ru");
        assertEquals("ru", techniqueCategory.getNameRu());

        assertEquals(techniqueCategory, techniqueCategory);
        assertNotEquals(0, techniqueCategory.hashCode());
        assertNotEquals(techniqueCategory, new TechniqueCategory());
    }
}

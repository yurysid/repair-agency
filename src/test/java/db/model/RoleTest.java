package db.model;

import agency.db.model.Role;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoleTest {
    @Test
    public void initRoleTest(){
        Role role = new Role();
        role.setId(1);
        assertEquals(1, role.getId());
        role.setNameEn("en");
        assertEquals("en", role.getNameEn());
        role.setNameRu("ru");
        assertEquals("ru", role.getNameRu());

        assertEquals(role, role);
        assertNotEquals(0, role.hashCode());
        assertNotEquals(role, new Role());
    }
}
